// https://github.com/michael-ciniawsky/postcss-load-config


const purgecss = require('postcss-purgecss')

module.exports = {
  "plugins": [
    require('postcss-import'),
    require('postcss-url'),
    // to edit target browsers: use "browserslist" field in package.json
    require('tailwindcss')('tailwind.config.js'),
    purgecss({
      content: ['./src/**/*.vue'],
      extractors: [
        {
          extractor: class TailwindExtractor {
            static extract(content) {
              return content.match(/[A-z0-9-:\/]+/g) || []
            }
          },
          extensions: ['vue'],
        },
      ],
    }),
    require('autoprefixer')()
  ]
}
