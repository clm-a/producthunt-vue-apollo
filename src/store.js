import Vue from 'vue'
import Vuex from 'vuex'
import graphqlClient from './utils/graphql_client'
import gql from 'graphql-tag'
Vue.use(Vuex)

const INDEX_QUERY = gql`
query Posts($after: String, $postedBefore: DateTime, $postedAfter: DateTime){
  posts(after: $after, postedAfter: $postedAfter, postedBefore: $postedBefore, first:5000){
    totalCount,
    pageInfo {
      endCursor
      hasNextPage
    }
    edges{
      node{
        id,
        name,
        tagline,
        url,
        thumbnailUrls: thumbnail{
          sm: url(width: 250, height: 250)
          md: url(width: 350, height: 350)
          lg: url(width: 500, height: 500)
        },
        votesCount,
        commentsCount,
        makers{
          id
        }
      }
    }
  }
}`

export default new Vuex.Store({
  state: {
    posts: []
  },
  mutations:{
    pushPosts(state, posts){
      state.posts.push(...posts)
    },
    clearPosts(state){
      state.posts = []
    }
  },
  actions:{
    clearPosts({commit}){
      commit('clearPosts')
    },
    loadPostsAtDate({commit}, formattedDate){

      let fromDate, toDate = null
      if(formattedDate){
        fromDate = new Date(formattedDate)
        fromDate.setHours(0,0,0,0)
        toDate = new Date(formattedDate);
        toDate.setHours(23,59,59,999)
      } else{
        fromDate = new Date();
        fromDate.setHours(0,0,0,0)
        toDate = new Date();
        toDate.setHours(23,59,59,999)
      }
      paginate(fromDate, toDate, commit)   
      
    }
  }
})



function paginate(fromDate, toDate, commit, after = null){
  let prom = graphqlClient.query({
    query: INDEX_QUERY,
    variables: {after: after, postedAfter: fromDate, postedBefore: toDate}
  })
  prom.then((resp) => {
    commit('pushPosts', resp.data.posts.edges.map(edge => edge.node))
    if(resp.data.posts.pageInfo.hasNextPage){
      paginate(fromDate, toDate, commit, resp.data.posts.pageInfo.endCursor)
    }
  })
  prom.catch(error => console.log(error))

}