import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import styles from './styles.css'

Vue.config.productionTip = false

Vue.filter('pluralize', (word, amount) => (amount > 1 || amount === 0) ? `${word}s` : word)

/* eslint-disable no-new */
new Vue({
  store,
  el: '#app',
  router,
  render: h => h(App)
})
