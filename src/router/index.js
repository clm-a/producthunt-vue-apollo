import Vue from 'vue'
import Router from 'vue-router'
import PostsIndex from '../components/Posts/Index'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: PostsIndex
    },
    {
      path: '/archive/:date',
      name: 'Archive',
      component: PostsIndex
    }
  ]
})
