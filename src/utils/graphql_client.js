import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

export default new ApolloClient({
  // Injected by Webpack DefinePlugin
  link: new HttpLink({ uri: GRAPHQL_API_URI }),
  cache: new InMemoryCache(),
});
