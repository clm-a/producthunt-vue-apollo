// tailwind.config.js
module.exports = {
  theme: {
    extend: {
      colors: {
        'primary-orange': '#E15336',
        'secondary-gray': '#8A8889',
        'charcoal': '#2F3D51'
  
      },
      screens:{
        '2xl': '1900px'
      }
    }
  },
  variants: {},
  plugins: [],
}
